# ANG PRIM
an Angular project using a template from PRIME-NG, and there's soo many help from other dependency too
## How To Run this Project
- First of all after you has clone this project, check your device to see if has a node package manager to do 'npm install' for install every dependency for this project.</br>
- Then after you finishing install the dependency, you can do 'npm start' or 'ng serve' (if you have angular in your device) to start the project</br>
- After successfully running the program, log in first by entering the username admin@mail.com and password admin123 then it will redirect to main page</br>
> the environtment we are using in this project is SIT env
## Tips
- For Accessing the detail page, you can select data row from the table </br>
- Action Button at right side of DataTable will perform action based on button type (it works for now) </br>
- The "View" button will show the details of selected item </br>
- If you want to add new Employee, please click Add button </br>
<hr>
