import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectMultipleComponent } from './select-multiple.component';

@NgModule({
  declarations: [SelectMultipleComponent],
  imports: [
    CommonModule
  ],
  exports: [
    SelectMultipleComponent
  ]
})
export class SelectMultipleModule { }
