import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-select-multiple',
  templateUrl: './select-multiple.component.html',
  styleUrls: ['./select-multiple.component.sass'],
})
export class SelectMultipleComponent implements OnChanges {
  @Input() inputData: any[];
  @Input() readOnly: boolean;
  @Input() inputMax: number;
  @Input() inputSelected: any[] = [];
  @Input() inputPlaceholder: string;
  @Input() reset = 0;
  @Output() selectedEvent = new EventEmitter<any>();
  @Output() eventDeleted = new EventEmitter<any>();
  public pureData: Format[];
  public data: Format[] = [
    { id: 1, name: 'Jakarta' },
    { id: 2, name: 'Bandung' },
    { id: 3, name: 'Surabaya' },
    { id: 4, name: 'Medan' },
  ];

  public displayListItem = false;
  public listSelected: any = [];
  public maxSelect = 10;
  public placeholder = '';

  private currentFocus: any = -1;
  private scroll = false;

  constructor() {}
  ngOnChanges() {
    this.listSelected = [];
    this.initInput();
  }

  initInput() {
    this.data = this.inputData;
    this.pureData = this.inputData;
    this.maxSelect = this.inputMax;
    this.inputSelected = this.inputSelected === null ? [] : this.inputSelected;
    this.inputSelected.map((x) => {
      this.data.map((y) => {
        if (y.id === x) {
          this.listSelected.push(y);
        }
      });
    });
  }

  focus() {
    this.displayListItem = true;
  }

  blur() {
    setTimeout(() => {
      this.displayListItem = false;
    }, 200);
  }

  keyup(ev: any) {
    switch (ev.keyCode) {
      case 38:
        this.currentFocus -= 1;
        this.addActive();
        break;
      case 40:
        this.currentFocus += 1;
        this.addActive();
        break;
      case 13:
        ev.preventDefault();
        const x = this.currentFocus;
        // if (x > -1) {
        // $(`#list-item-${x}`).click();
        const id = $('.ds-selectize-list').children('div:visible')[x].id;
        $(`#${id}`).click();
        // }
        break;
    }
  }

  keydown(ev: any) {
    if (ev.keyCode === 8) {
      const keyword = ev.target.value;
      const i = this.listSelected.length - 1;
      if (!keyword) {
        this.deleteSelected(this.listSelected[i].id);
      }
    }
  }

  addActive() {
    if (this.currentFocus >= this.data.length) {
      this.currentFocus = 0;
    }
    if (this.currentFocus < 0) {
      this.currentFocus = this.data.length - 1;
    }
    const x = this.currentFocus;
    $('.ds-selectize-list-item').removeClass('ds-selectize-list-item-active');
    const id = $('.ds-selectize-list').children('div:visible')[x].id;
    if ($(`#${id}`)) {
      $(`#${id}`).addClass('ds-selectize-list-item-active');
    }
  }

  input(ev: any) {
    const keyword = ev.target.value;
    const keywordLower = keyword.toLowerCase();
    if (keyword) {
      this.data = this.pureData.filter((x) => x.name.includes(keywordLower))
      this.addActive();
      // $('.ds-selectize-list-item').filter(() => {
      //   $(this).toggle($(this).text().toLowerCase().indexOf(keywordLower) > -1);
      //   this.addActive();
      // });
      this.currentFocus = -1;
    } else {
      $(`.ds-selectize-list-item`).show();
    }
 }

  click(d) {
    if (this.listSelected.length < this.maxSelect) {
      const isExist = this.listSelected.filter((x) => x.id === d.id);
      if (isExist.length === 0) {
        this.listSelected.push(d);
      }
      this.selectedEvent.emit(this.listSelected);
      this.placeholder = '';
      // const n = this.listSelected.length * 80;
      // $('.ds-selectize-input').css('width', `calc(100% - ${n}px)`);
    }
  }

  deleteSelected(id: any) {
    const removeIndex = this.listSelected.map((item) => item.id).indexOf(id);
    this.listSelected.splice(removeIndex, 1);
    this.selectedEvent.emit(this.listSelected);
    if (this.listSelected === 0) {
      this.placeholder = this.inputPlaceholder;
    }
  }

  deleteSelected2(id: any) {
    this.eventDeleted.emit(id);
  }

  autoFocus(element: any) {
    // element.blur();
    element.focus();
  }
}

class Format {
  id: number;
  name: string;
  extra?: any = {};
}
