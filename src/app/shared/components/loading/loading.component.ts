import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(private readonly router:Router) { }

  items: any;
  ngOnInit(): void {
  }

  onLogOut(){
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
    setTimeout(() => {
      location.reload()
    }, 80);
  }

  isLoggedIn(): boolean{
    return localStorage.getItem('token') !== null;
  }

}
