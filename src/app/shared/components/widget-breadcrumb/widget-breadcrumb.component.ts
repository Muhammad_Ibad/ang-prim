import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-widget-breadcrumb',
  templateUrl: './widget-breadcrumb.component.html',
  styleUrls: ['./widget-breadcrumb.scss']
})
export class WidgetBreadcrumbComponent implements OnInit {
  constructor() { }

  public items: MenuItem[] = []

  @Input() model: any;
  ngOnInit() {
    this.model.map(item => {
      this.items.push(
        {label: item.label, icon: item.icon, url: item.url, routerLink: item.link}
      );
    });
  }
}
