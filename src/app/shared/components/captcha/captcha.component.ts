import { Component, ViewChild, ElementRef, Input, Output, EventEmitter, ChangeDetectorRef, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.css']
})
export class CaptchaComponent implements OnDestroy {
  @ViewChild('captchaCanvas') captchaCanvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;
  
  @Input() captchaVerified: boolean;
  @Output() statusVerified = new EventEmitter<boolean>();

  robotOptions: any[];
  showCaptcha: boolean = false;
  captchaCode: string = '';
  userInput: string = '';
  userInputMessage: string = '';

  userInputValid: boolean;
  userInputTouched: boolean = false;

  displayCaptchaDialog: boolean = false;
  captchaInput: string = '';

  constructor(private cdr: ChangeDetectorRef) {
  }

  openCaptchaDialog() {
    if (this.captchaVerified) {      
      this.displayCaptchaDialog = true;
      this.userInputTouched = false;
      this.refreshCaptcha();
    } else {
      this.displayCaptchaDialog = false;
    }
    this.cdr.detectChanges(); // Memaksa deteksi perubahan setelah perubahan nilai
  }

  refreshCaptcha() {
    // Logic to generate a new CAPTCHA code and draw it on the canvas
    this.userInput = '';
    this.captchaCode = this.generateRandomCode();
    this.drawCaptcha(this.captchaCode);
  }

  drawCaptcha(code: string) {
    this.ctx = this.captchaCanvas.nativeElement.getContext('2d');
    this.ctx.clearRect(0, 0, this.captchaCanvas.nativeElement.width, this.captchaCanvas.nativeElement.height);
    this.ctx.font = 'italic 60px Lobster, Pacifico, Amatic SC, Orbitron, VT323';
    this.ctx.fillText(code, 20, 60);

    // Mendapatkan metrik teks
    const textMetrics = this.ctx.measureText(code);
    
    // Menggambar garis melintang
    this.ctx.beginPath();
    this.ctx.moveTo(20, 40); // Koordinat awal garis
    this.ctx.lineTo(20 + textMetrics.width, 40); // Koordinat akhir garis sesuai panjang teks
    this.ctx.stroke();    
  }  

  verifyCaptcha() {
    this.cdr.detectChanges(); // Memaksa deteksi perubahan setelah perubahan nilai    
    if (this.userInput.toLowerCase() === this.captchaCode.toLowerCase()) {
      this.displayCaptchaDialog = false;
      this.captchaVerified = true;
      this.statusVerified.emit(true);
    } else {
      this.userInputValid = false;
      this.userInputTouched = true;
      this.userInputMessage = 'Captcha tidak sesuai, Harap Coba Kembali.';
      this.captchaVerified = false;
      this.statusVerified.emit(false);
      this.userInput = '';
      this.refreshCaptcha();
    }
  }

  generateRandomCode() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let code = '';
    for (let i = 0; i < 6; i++) {
      code += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return code;
  }

  ngOnDestroy() {
    this.statusVerified.unsubscribe(); // Membersihkan langganan EventEmitter
  }
}
