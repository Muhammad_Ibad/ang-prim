import Swal from "sweetalert2";

export function loadingScreen(value: boolean) {
    let loading;
    if (value) {
        loading = Swal.fire({
            html: '<i class="pi pi-spin pi-spinner" style="font-size: 3em; color: #fff;"></i><p style="color: #fff">please wait...</p>',
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false,
            showClass: {
                popup: 'swal2-no-animation'
            },
            position: 'center',
            background: 'transparent'
        });
    }
    if (!value) {
        loading = Swal.close();
    }
    return loading;
}