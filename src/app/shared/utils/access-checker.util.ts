import { prepareUserData } from "src/app/auth/auth.component";

export async function accessChecker(pageName: string) {
    const userData = await prepareUserData();
    if (userData.userPage.find(item => item.includes(pageName)) != undefined) {
        return true;
    } else {
        return false;
    }
}
