import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { PrimengModule } from './primeng/primeng.module';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { WidgetBreadcrumbComponent } from './components/widget-breadcrumb/widget-breadcrumb.component';
import { BlockUIModule } from 'primeng/blockui';
import { ValidationMessageComponent } from './components/validation-message/validation-message.component';
import { InputTextModule } from 'primeng/inputtext';
import { CaptchaComponent } from './components/captcha/captcha.component';
import { SelectButtonModule } from 'primeng/selectbutton';
@NgModule({
  declarations: [
    HeaderComponent,
    WidgetBreadcrumbComponent,
    ValidationMessageComponent,
    CaptchaComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    InputTextModule,
    SelectButtonModule,
    PrimengModule,
    BreadcrumbModule,
    BlockUIModule
  ],
  exports: [
    PrimengModule,
    HeaderComponent,
    WidgetBreadcrumbComponent,
    ValidationMessageComponent,
    CaptchaComponent
  ]
})
export class SharedModule { }
