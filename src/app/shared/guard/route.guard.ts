import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouteGuard implements CanActivate {
  constructor(private readonly router:Router){

  }
  canActivate() : boolean {
    return this.authorize();
  }
  canActivateChild() : boolean {
    return this.authorize();
  }

  private authorize(): boolean{
    const authorize: boolean = (localStorage.getItem('session-token') !== null)
    if (!authorize) {
      this.router.navigateByUrl('/unauthorize');
    }
    return authorize;
  }

}
