import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private readonly router:Router) {}
  private handleError(error:HttpErrorResponse): Observable<any>{

    if (error.status === 401 || error.status === 403) {
      Swal.fire({
        title: 'Error',
        text: `Terdapat Error.`,
        icon: 'error',
        showCancelButton: false,
      })
      // alert(`Terjadi Error ${error.status} ${error.statusText}`);
      this.router.navigateByUrl('auth/logout')
    }

    return throwError(error)
  };
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // console.log(`Intercept ${request.method} request to ${request.url}`);
    const sessionToken: string = localStorage.getItem('session-token') as string;
    let newHeaders = request.headers;
    if (sessionToken) {
      newHeaders = newHeaders.append('session-token', `${sessionToken}`);
    }
    const newRequest = request.clone({ headers: newHeaders });
    return next.handle(newRequest).pipe(catchError(err => this.handleError(err)));
  }
}
