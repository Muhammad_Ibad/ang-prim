export const locationList = [
    {
      label: 'Bali',
      value: 'Bali'
    },
    {
      label: 'Bangka Belitung',
      value: 'Bangka Belitung'
    },
    {
      label: 'Banten',
      value: 'Banten'
    },
    {
      label: 'Bengkulu',
      value: 'Bengkulu'
    },
    {
      label: 'Daerah Istimewa Yogyakarta',
      value: 'Daerah Istimewa Yogyakarta'
    },
    {
      label: 'DKI Jakarta',
      value: 'DKI Jakarta'
    },
    {
      label: 'Gorontalo',
      value: 'Gorontalo'
    },
    {
      label: 'Jambi',
      value: 'Jambi'
    },
    {
      label: 'Jawa Barat',
      value: 'Jawa Barat'
    },
    {
      label: 'Jawa Tengah',
      value: 'Jawa Tengah'
    },
    {
      label: 'Jawa Timur',
      value: 'Jawa Timur'
    },
    {
      label: 'Kalimantan Barat',
      value: 'Kalimantan Barat'
    },
    {
      label: 'Kalimantan Selatan',
      value: 'Kalimantan Selatan'
    },
    {
      label: 'Kalimantan Tengah',
      value: 'Kalimantan Tengah'
    },
    {
      label: 'Kalimantan Timur',
      value: 'Kalimantan Timur'
    },
    {
      label: 'Kalimantan Utara',
      value: 'Kalimantan Utara'
    },
    {
      label: 'Kepulauan Riau',
      value: 'Kepulauan Riau'
    },
    {
      label: 'Lampung',
      value: 'Lampung'
    },
    {
      label: 'Maluku',
      value: 'Maluku'
    },
    {
      label: 'Maluku Utara',
      value: 'Maluku Utara'
    },
    {
      label: 'Nanggroe Aceh Darussalam',
      value: 'Nanggroe Aceh Darussalam'
    },
    {
      label: 'Nusa Tenggara Barat',
      value: 'Nusa Tenggara Barat'
    },
    {
      label: 'Nusa Tenggara Timur',
      value: 'Nusa Tenggara Timur'
    },
    {
      label: 'Papua',
      value: 'Papua'
    },
    {
      label: 'Papua Barat',
      value: 'Papua Barat'
    },
    {
      label: 'Papua Barat Daya',
      value: 'Papua Barat Daya'
    },
    {
      label: 'Papua Pegunungan',
      value: 'Papua Pegunungan'
    },
    {
      label: 'Papua Selatan',
      value: 'Papua Selatan'
    },
    {
      label: 'Papua Tengah',
      value: 'Papua Tengah'
    },
    {
      label: 'Riau',
      value: 'Riau'
    },
    {
      label: 'Sulawesi Barat',
      value: 'Sulawesi Barat'
    },
    {
      label: 'Sulawesi Selatan',
      value: 'Sulawesi Selatan'
    },
    {
      label: 'Sulawesi Tengah',
      value: 'Sulawesi Tengah'
    },
    {
      label: 'Sulawesi Tenggara',
      value: 'Sulawesi Tenggara'
    },
    {
      label: 'Sulawesi Utara',
      value: 'Sulawesi Utara'
    },
    {
      label: 'Sumatera Barat',
      value: 'Sumatera Barat'
    },
    {
      label: 'Sumatera Selatan',
      value: 'Sumatera Selatan'
    },
    {
      label: 'Sumatera Utara',
      value: 'Sumatera Utara'
    },
  ]

export const fuelList = [
    {
        label: 'Bensin',
        value: 'Bensin'
    },
    {
        label: 'Diesel',
        value: 'Diesel'
    },
    {
        label: 'Listrik',
        value: 'Listrik'
    },
    {
        label: 'Hybrid',
        value: 'Hybrid'
    },
]

export const transmissionList = [
    {
        label: 'Automatic',
        value: 'Automatic'
    },
    {
        label: 'Manual',
        value: 'Manual'
    }
]

export function generateYearOptions(): number[] {
    const currentYear = new Date().getFullYear();
    const tenYearsAgo = currentYear - 10;
    const yearArray: any[] = [];
    for (let year = currentYear; year >= tenYearsAgo; year--) {
      yearArray.push({label:`${year}`, value:`${year}`});
    }
    return yearArray;
}