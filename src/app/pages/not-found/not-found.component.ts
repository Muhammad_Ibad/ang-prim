import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }
  errorCode = ''
  errorQuote = ''
  errorMessage = ''
  hasLogin = false;
  ngOnInit() {
    this.activatedRoute.params
    .pipe(map((params:any)=> params.errorPath))
    .subscribe((errorPage)=> {
      if (errorPage == 'unauthorize') {
        this.errorCode = '401'
        this.errorQuote = `UH OH! Stop Right There!`
        this.errorMessage = 'The page you are looking for is Restricted. But you can click the button below to go back.'
      } else {
        this.errorCode = '404'
        this.errorQuote = `UH OH! You're lost.`
        this.errorMessage = `The page you are looking for does not exist. How you got here is a mystery. But you can click the button below to go back.`
      }
    })
    
    if (localStorage.getItem('session-token')) {
      this.hasLogin = true;
    } else {
      this.hasLogin = false;
      this.router.navigateByUrl('/login');
    }
  }

  back(){
    history.back();
  }
  login(){
    this.router.navigateByUrl('/login');
  }
}
