import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { NotFoundComponent } from './not-found.component';
import { NotFoundRoutingModule } from './not-found-routing.module';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    NotFoundRoutingModule,
    CardModule,
    ButtonModule,
    SharedModule,
    CarouselModule,
    ProgressSpinnerModule,
  ]
})
export class NotFoundModule { }