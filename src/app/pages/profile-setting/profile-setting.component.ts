import { Component, OnInit } from '@angular/core';
import { ProfileSettingService } from 'src/app/service/profile-setting.service';
import Swal from 'sweetalert2';
import * as _ from 'lodash'
import { prepareUserData } from 'src/app/auth/auth.component';

@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.component.html',
  styleUrls: ['./profile-setting.component.scss']
})
export class ProfileSettingComponent implements OnInit {

  constructor(
    private profileSettingService: ProfileSettingService
  ) { }

  breadcrumbModel = [
    {label:'Applications', icon:'', url:'/'},
    {label:'Setting', icon:'pi pi-fw pi-cog', url:'/profile-setting'},
  ]

  dataUserDetail: any;
  clonedUserDetail: any;
  dataUserPassword = {password: '',  confirmPassword: '' }
  
  disabledSubmitButton: boolean = true;
  disabledConfirmPassword: boolean = true;

  errorEmail: boolean = true;
  expression: RegExp = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;

  errorPassword: boolean = true;
  userData: any = {}
  async ngOnInit() {
    this.userData = await prepareUserData();
    this.submitSearchPeople();
  }

  async submitSearchPeople() {
    const dataRequest = {
      id: this.userData.id,
    };
    
    // loadingScreen(true);
    await this.
      profileSettingService.GetListUserById(dataRequest)
      .toPromise()
      .then((res) => {
        this.dataUserDetail = res.data;
        this.clonedUserDetail = _.cloneDeep(res.data);
        this.disabledSubmitButton = true;
        // loadingScreen(false);
      })
      .catch((res) => {
        this.dataUserDetail = res.data;
        this.disabledSubmitButton = true;
        Swal.fire({
          showCloseButton: true,
          titleText: 'Warning',
          icon: 'warning',
          text:
            res.statusText,
        });
      });
  }

  onChangeSubmitEditPeople() {
    if (this.dataUserPassword.password) {
      this.disabledConfirmPassword = false;
      this.onChangeWithPassword();
    } else {
      this.disabledConfirmPassword = true;
      this.dataUserPassword.confirmPassword = "";
      this.errorPassword = true;
      this.onChangeWithoutPassword();
    }
  }

  onChangeWithPassword() {
    if (this.dataUserDetail.username
      && this.dataUserDetail.display_name
      && (this.expression.test(this.dataUserDetail.email))
      && this.dataUserPassword.password
      && this.dataUserPassword.confirmPassword
      && (this.dataUserPassword.password === this.dataUserPassword.confirmPassword)) {
        this.disabledSubmitButton = false;
      } else {
        this.disabledSubmitButton = true;
      }
  }

  onChangeWithoutPassword() {
    if (this.dataUserDetail.username
        && this.dataUserDetail.display_name
        && (this.expression.test(this.dataUserDetail.email))
        && (JSON.stringify(this.dataUserDetail) !== JSON.stringify(this.clonedUserDetail))) {
      this.disabledSubmitButton = false;
    } else {
      this.disabledSubmitButton = true;
    }
  }

  onChangeEmailInput() {
    if (this.expression.test(this.dataUserDetail.email)) {
      this.errorEmail = true;
    } else {
      this.errorEmail = false;
    }
    this.onChangeSubmitEditPeople();
  }

  onChangePasswordInput() {
    if (this.dataUserPassword.password == this.dataUserPassword.confirmPassword) {
      this.errorPassword = true;
    } else {
      this.errorPassword = false;
    }
    this.onChangeSubmitEditPeople();
  }

  onPaste(event: ClipboardEvent): void {
    event.preventDefault();
  }

  onClickSubmit() {
    Swal.fire({
      title: 'Enter Password',
      input: 'password',
      inputPlaceholder: "Enter your password",
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off"
      },
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Submit',
    }).then((result) => {
      if (result.isConfirmed) {
        this.submitEdit(result.value);
      }
    });
  }

  submitEdit(password: any) {
    const dataRequest = {
      id: this.dataUserDetail.id,
      username: this.dataUserDetail.username ? this.dataUserDetail.username : "",
      old_password: password,
      new_password: this.dataUserPassword.password ? this.dataUserPassword.password : "",
      email: this.dataUserDetail.email ? this.dataUserDetail.email : "",
      display_name: this.dataUserDetail.display_name ? this.dataUserDetail.display_name : "",
    };
    this.profileSettingService
      .GetUpdateUser(dataRequest)
      .toPromise()
      .then((res) => {
        console.log(res);
      });

    Swal.fire({
      title: 'Success',
      text: `Successfully update user for ${this.dataUserDetail.display_name}`,
      icon: 'success',
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Okay',
    }).then((result) => {
      if (result) {
        this.submitSearchPeople();
      }
    });
  }
}
