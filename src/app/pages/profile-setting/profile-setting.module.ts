import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProfileSettingComponent } from './profile-setting.component';
import { ProfileSettingRoutingModule } from './profile-setting-routing.module';

@NgModule({
  declarations: [
    ProfileSettingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    CardModule,
    ProfileSettingRoutingModule,
    SharedModule,
    CarouselModule,
    ProgressSpinnerModule,
  ]
})
export class ProfileSettingModule { }