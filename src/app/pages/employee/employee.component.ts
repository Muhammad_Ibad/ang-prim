import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { map } from 'rxjs/operators';
import { accessChecker } from 'src/app/shared/utils/access-checker.util';
import { loadingScreen } from 'src/app/shared/utils/loading-screen.util';
import { SortEvent } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private messageService: MessageService,
    private router: Router
  ) { }

  breadcrumbModel = [
    {label:'Applications', icon:'', url:'/'},
    {label:'Employee', icon:'pi pi-fw pi-home', url:'/'},
  ]

  dataEmployees = [];
  clonedEmployee: any = {};
  editing = true
  searchValue = ''

  statusOptions: any[] = [
    { label: 'Active', value: 'Active' },
    { label: 'Non-Active', value: 'Non-Active' }
  ];

  @ViewChild('dataTableEmployee') dataTableEmployee: Table;

  async ngOnInit() {
    await this.getAllDataEmployee().then(() => {
      const searchValue: string = localStorage.getItem('searchValue') as string;
      if (searchValue) {
        this.searchValue = searchValue;
        this.dataTableEmployee.filterGlobal(searchValue, 'contains');
      }
    });
  }

  async getAllDataEmployee() {
    await this.employeeService
      .getData()
      .toPromise()
      .then((res) => {
        this.dataEmployees = res.data.employee;
      });
  }

  onRowEditInit(employee: any) {
    this.clonedEmployee[employee.email] = {...employee};
  }

  onRowEditSave(employee: any, index: any) {
    this.dataEmployees[index].status = employee.status.value;
    delete this.clonedEmployee[employee.email];
    this.messageService.add({severity:'success', summary: 'Success', detail:'Berhasil Update data employe'});
  }

  onRowEditCancel(employee: any, index: any) {
    this.dataEmployees[index] = this.clonedEmployee[employee.email];
    delete this.clonedEmployee[employee.email];
  }

  onRowDelete(employee: any, index: any) {
    this.dataEmployees = this.dataEmployees.filter(x => x.email != employee.email);
    this.messageService.add({severity:'success', summary: 'Success', detail:'Berhasil Hapus data employee'});
  }

  onRowSelect(data: any) {
    this.router.navigate(['employee/', data.email]);
  }

  onSearchInputChange(event: Event) {
    const inputValue = (event.target as HTMLInputElement).value;
    this.dataTableEmployee.filterGlobal(inputValue, 'contains');
    localStorage.setItem('searchValue', inputValue);
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
        let value1 = data1[event.field];
        let value2 = data2[event.field];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order * result);
    });
  }

}
