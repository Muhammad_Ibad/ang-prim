import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './employee.component';
import { AddEmployeeComponent } from './add/add-employee.component';
import { DetailEmployeeComponent } from './detail/detail-employee.component';
import { AppLayoutNotFoundComponent } from 'src/app/layout/app.layout.notfound.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeComponent,
  },
  {
    path: 'employee/add',
    component: AddEmployeeComponent,
  },
  {
    path: 'employee/:email',
    component: DetailEmployeeComponent,
  },
  {
    path: '***',
    component: AppLayoutNotFoundComponent,
    children : [
      { path: '', loadChildren: () => import('../not-found/not-found.module').then(m => m.NotFoundModule) }, 
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}
