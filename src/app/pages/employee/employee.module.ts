import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardModule } from 'primeng/card';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { EmployeeComponent } from './employee.component';
import { EmployeeRoutingModule } from './employee-routing.module';
import { AddEmployeeComponent } from './add/add-employee.component';
import { DetailEmployeeComponent } from './detail/detail-employee.component';
import { RippleModule } from 'primeng/ripple';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    EmployeeComponent,
    AddEmployeeComponent,
    DetailEmployeeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmployeeRoutingModule,
    PrimengModule,
    SharedModule,
    CardModule,
    RippleModule,
    HttpClientModule,
  ]
})
export class EmployeeModule { }