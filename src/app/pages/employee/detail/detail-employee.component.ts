import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { map } from 'rxjs/operators';
import { accessChecker } from 'src/app/shared/utils/access-checker.util';
import { loadingScreen } from 'src/app/shared/utils/loading-screen.util';

@Component({
  selector: 'app-detail-employee',
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.scss']
})
export class DetailEmployeeComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  breadcrumbModel = [
    {label:'Applications', icon:'', url:'/'},
    {label:'Dashboard', icon:'pi pi-fw pi-home', url:'/'},
  ]

  dataEmployees = [];
  dataEmployee: any = {};

  emailEmployee = '';

  async ngOnInit() {
    this.route.params.subscribe(params => {
      this.emailEmployee = params['email'];
    })
    await this.getAllDataEmployee().then(() => {
      this.dataEmployee = this.dataEmployees.find(x => x.email === this.emailEmployee)
      console.log(this.dataEmployee);
    });
  }

  async getAllDataEmployee() {
    await this.employeeService
      .getData()
      .toPromise()
      .then((res) => {
        this.dataEmployees = res.data.employee;
      });
  }

  backToList() {
    this.router.navigateByUrl('/');
  }

}
