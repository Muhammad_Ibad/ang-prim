import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { map } from 'rxjs/operators';
import { accessChecker } from 'src/app/shared/utils/access-checker.util';
import { loadingScreen } from 'src/app/shared/utils/loading-screen.util';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) { }

  breadcrumbModel = [
    {label:'Applications', icon:'', url:'/'},
    {label:'Dashboard', icon:'pi pi-fw pi-home', url:'/'},
  ]

  dataEmployees = [];
  frmEmployee: any = {
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalry: '',
    group: '',
  };
  maxDate: Date = new Date();

  optGroup = [
    {value:  "Group 1", label: "Group 1"},
    {value:  "Group 2", label: "Group 2"},
    {value:  "Group 4", label: "Group 4"},
    {value:  "Group 5", label: "Group 5"},
    {value:  "Group 6", label: "Group 6"},
    {value:  "Group 7", label: "Group 7"},
    {value:  "Group 8", label: "Group 8"},
    {value:  "Group 9", label: "Group 9"},
    {value:  "Group 10", label: "Group 10"},
  ]

  usernameValid: boolean;
  firstNameValid: boolean;
  lastNameValid: boolean;
  emailValid: boolean;
  birthDateValid: boolean;
  basicSalaryValid: boolean;
  statusValid: boolean;
  groupValid: boolean;

  usernameTouched: boolean = false;
  firstNameTouched: boolean = false;
  lastNameTouched: boolean = false;
  emailTouched: boolean = false;
  birthDateTouched: boolean = false;
  basicSalaryTouched: boolean = false;
  statusTouched: boolean = false;
  groupTouched: boolean = false;

  usernameMessage = "";
  firstNameMessage = "";
  lastNameMessage = "";
  emailMessage = "";
  birthDateMessage = "";
  basicSalaryMessage = "";
  groupMessage = "";


  async ngOnInit() {
    
  }

  onCancel() {
    this.router.navigateByUrl('/')
  }

  async onSubmit() {
    if (this.usernameValid && this.lastNameValid && this.firstNameValid && this.emailValid && this.birthDateValid && this.basicSalaryValid && this.groupValid) {
      Swal.fire({
        title: 'Lanjutkan Simpan Data',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancel',
        confirmButtonText: 'Submit',
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigateByUrl("/")
        }
      });
    } else {
      this.usernameTouched = true;
      this.firstNameTouched = true;
      this.lastNameTouched = true;
      this.emailTouched = true;
      this.birthDateTouched = true;
      this.groupTouched = true;
      this.basicSalaryTouched = true;
      this.validateUsername();
      this.validateFirstName();
      this.validateLastName();
      this.validateEmail();
      this.validateBasicSalary();
      this.validateBirthDate();
      this.validateGroup();
    }
  }


  //#region Validation
  validateUsername() {
    if (!this.frmEmployee.firstName && this.frmEmployee.firstName.length < 1) {
      this.usernameValid = false;
      this.usernameMessage = 'Username harus di isi.';
    }else{
      this.usernameValid = true;
    }//  else if() {
    //   this.username.message = 'Username harus di isi.';
    // }
  }

  validateFirstName() {
    if (!this.frmEmployee.firstName && this.frmEmployee.firstName.length < 1) {
      this.firstNameValid = false;
      this.firstNameMessage = 'First Name harus di isi.';
    } else {
      this.firstNameValid = true;
    }
  }

  validateLastName() {
    if (!this.frmEmployee.lastName && this.frmEmployee.lastName.length < 1) {
      this.lastNameValid = false;
      this.lastNameMessage = 'Last name harus di isi.';
    } else {
      this.lastNameValid = true;
    }
  }

  validateEmail() {
    if (!this.frmEmployee.email && this.frmEmployee.email.length < 1) {
      this.emailValid = false;
      this.emailMessage = 'Email harus di isi.';
    } else {
      const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
      this.emailValid = emailPattern.test(this.frmEmployee.email);
      if (this.emailValid) {
        this.emailMessage = '';
      } else {
        this.emailMessage = 'Tolong masukkan email yang valid';
      }
    }
  }

  isEmailValid(email: string): boolean {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailPattern.test(email);
  }

  validateBirthDate(): any {
    if (!this.frmEmployee.birthDate && this.frmEmployee.birthDate === '') {
      this.birthDateValid = false;
      this.birthDateMessage = 'Birth Date harus di pilih.';
    } else {
      const today = new Date();
      if (this.frmEmployee.birthDate <= today) {
        this.birthDateValid = true;        
      } else {
        this.birthDateValid = false;
      }
    }
  }

  validateBasicSalary() {
    if (this.frmEmployee.basicSalary === null || this.frmEmployee.basicSalary === undefined || this.frmEmployee.basicSalary === '') {
      this.basicSalaryValid = false;
      this.basicSalaryMessage = 'Basic Salary harus di isi.';
    } else {
      this.basicSalaryValid = true;
    }
  }

  validateGroup() {
    if (!this.frmEmployee.group && this.frmEmployee.group === '') {
      this.groupValid = false;
      this.groupMessage = 'Grup harus di pilih.';
    } else {
      this.groupValid = true;
    }
  }

  isGroupValid(group: string): boolean {
    return group !== '';
  }
}
//#endregion