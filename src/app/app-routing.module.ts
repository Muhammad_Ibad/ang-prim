import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './layout/app.layout.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { RouteGuard } from './shared/guard/route.guard';
import { AppLayoutNotFoundComponent } from './layout/app.layout.notfound.component';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    canActivate:[RouteGuard],
    children: [
      { path: '', loadChildren: () => import('./pages/employee/employee.module').then(m => m.EmployeeModule) },
      { path: 'profile-setting', loadChildren: () => import('./pages/profile-setting/profile-setting.module').then(m => m.ProfileSettingModule) },
    ]
  },
  {
    path: '',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: '',
    component: AppLayoutNotFoundComponent,
    children : [
      { path: '', loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule) }, 
    ]
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
