import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from '../service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { accessChecker } from '../shared/utils/access-checker.util';
import { loadingScreen } from '../shared/utils/loading-screen.util';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  providers: [MessageService]
})
export class AuthComponent implements OnInit {

  constructor(private messageService: MessageService, private readonly authService:AuthService, private readonly router:Router, private readonly activatedRoute:ActivatedRoute) { }

  usernameValid: boolean;
  passwordValid: boolean;
  captchaValid: boolean;

  usernameTouched: boolean = false;
  passwordTouched: boolean = false;

  passwordMessage = "";
  usernameMessage = "";

  isCheck: boolean = false;

  data: any;
  credentials: FormGroup = new FormGroup({
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required]),
  });

  loading: boolean = false;
  
  dataLogin = {username_email:'', password:''}
  rememberPass = false;

  ngOnInit() {
    this.activatedRoute.params
    .pipe(map((params:any)=> params.action))
    .subscribe((action)=> {
      if (action == 'logout') {
        localStorage.clear();
        this.router.navigateByUrl('/login')
      } else if (localStorage.getItem('session-token')) {
        if (accessChecker('Dashboard')) {
          this.router.navigateByUrl('/');
        } else {
          this.router.navigateByUrl('/phpm-monitoring');
        }
      }
    })
  }

  async onSignIn(){
    if (this.passwordValid && this.usernameValid) {
      loadingScreen(true);
      await this.authService.signIn(this.dataLogin).then((res) => {
        localStorage.setItem('userInfo', JSON.stringify(res.data));
        localStorage.setItem('session-token', res.data.session_token);
      }).then((res) => {
        this.router.navigateByUrl('/');
      }).catch((res) => {
        this.usernameTouched = true;
        this.passwordTouched = true;
        this.dataLogin.username_email = '';
        this.dataLogin.password = '';
        this.validateUsername();
        this.validatePassword();
        this.usernameMessage = 'Username atau Password tidak valid.';
        this.passwordMessage = 'Username atau Password tidak valid.';
      }).finally(() => loadingScreen(false))
    } else {
      this.usernameTouched = true;
      this.passwordTouched = true;
      this.validateUsername()
      this.validatePassword()
    }
    
    // this.storage.setItem('credentials', JSON.stringify(this.credentials))
    // console.log('Login value : ', this.credentials.value);
    // // this.data = this.credentials.value;
    // if (this.credentials.valid) {
    //   this.authService
    //   .sigin(this.credentials.value)
    //   .subscribe((response:any) => {
    //     localStorage.setItem('token', response.token),
    //     localStorage.setItem('username', this.credentials.get('username')?.value)
    //   }, console.error)
    //   // this.storage.setItem('credentials', JSON.stringify(this.credentials.value))
      // this.router.navigateByUrl('/')
    // }
  }

  forgetPassClick() {
    this.router.navigateByUrl('/');
  }

  setVerifiedCaptcha(data: boolean) {
    this.captchaValid = data;
  }

  validateUsername() {
    if (!this.dataLogin.username_email && this.dataLogin.username_email.length < 1) {
      this.usernameValid = false;
      this.usernameMessage = 'Username harus di isi.';
    }else{
      this.usernameValid = true;
    }//  else if() {
    //   this.username.message = 'Username harus di isi.';
    // }
  }

  validatePassword() {
    if (!this.dataLogin.password && this.dataLogin.password.length < 1) {
      this.passwordValid = false;
      this.passwordMessage = 'Password harus di isi.';
    } else {
      this.passwordValid = true;
    }
  }

}

export async function prepareUserData() {
	const userInfo = localStorage.getItem('userInfo');
	if (userInfo) {
		const userAD = JSON.parse(userInfo);    
		const responseData = {
			userFullName: userAD.display_name,
			userBranch: userAD.branch + '-' + userAD.branchName,
			userGroup: userAD.groupname,
      userPage: userAD.pagename,
			lastLogin: userAD.lastLogin,
			...userAD
		};
		return responseData;
	}
}