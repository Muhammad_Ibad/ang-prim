import { Component, ElementRef, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from "./service/app.layout.service";
import { Router } from '@angular/router';
import { prepareUserData } from 'src/app/auth/auth.component';
import { accessChecker } from 'src/app/shared/utils/access-checker.util';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { formatDistanceToNow } from 'date-fns';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {
  notificationArray: any[] = [];

  displayNotification: boolean = false;

  displayNotificationButton() {
    this.displayNotification = true;
  }

  cutArrayNotification(data: any) {
    this.notificationArray.splice(data, 1);
  }

    items!: MenuItem[];
    
    profileItems: MenuItem[] = [
        {
            label: 'Profile',
            items: [
                {
                    label: 'Setting',
                    icon: 'pi pi-cog',
                    routerLink: '/profile-setting'
                },
                {
                    label: 'Log Out',
                    icon: 'pi pi-sign-out',
                    command: () => {
                      localStorage.removeItem('session-token')
                      localStorage.removeItem('userInfo');
                      this.router.navigateByUrl('/auth/logout');
                      setTimeout(() => {
                          location.reload()
                      }, 150);
                    }
                }
            ]
        }
    ];
    
    userData: any = {}

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService, private readonly router:Router) { }

    private intervalSubscription: Subscription;

    async ngOnInit() {
      this.userData = await prepareUserData();

      if (this.userData.notification) {
        for (let i = 0; i < this.userData.notification.length; i++) {
          this.userData.notification[i].message = this.transformText(this.userData.notification[i].action_type);
          this.userData.notification[i].messageDetail1 = "Asset: "+this.userData.notification[i].mtf_assetcode;
          this.userData.notification[i].messageDetail2 = "Requestor: "+this.userData.notification[i].requestor_name;
          this.userData.notification[i].time = this.convertDateToAgoFormat(this.userData.notification[i].request_date);
        }
        this.notificationArray = this.userData.notification;
      }

      if (this.notificationArray.length > 0) {
        this.intervalSubscription = interval(60000)
        .subscribe(() => {
          this.customFunction();
        });
      }
    }

    ngOnDestroy() {
      if (this.intervalSubscription) {
        this.intervalSubscription.unsubscribe();
      }
    }

    customFunction() {
      for (let i = 0; i < this.notificationArray.length; i++) {
        this.notificationArray[i].time = this.convertDateToAgoFormat(this.notificationArray[i].request_date);
      }
    }

    convertDateToAgoFormat(input: any): string {
      const originalDate = new Date(input);
      return formatDistanceToNow(originalDate, { addSuffix: true });
    }

    transformText(input: any): string {
      return input
      .toLowerCase()  // Convert the entire string to lowercase
      .replace(/\b\w/g, (match) => match.toUpperCase())+" Request";
    }
    
    onLogOut(){
        localStorage.removeItem('session-token')
        localStorage.removeItem('userInfo');
        this.router.navigateByUrl('/auth/logout');
        setTimeout(() => {
            location.reload()
        }, 150);
    }
}
