import { Component, ElementRef, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from "./service/app.layout.service";
import { Router } from '@angular/router';

@Component({
    selector: 'app-topbar-notfound',
    templateUrl: './app.topbar.notfound.component.html'
})
export class AppTopBarNotFoundComponent {
    notificationArray: any[] = [
    {
      message: 'Pengajuan Status Overwrite Ditolak',
      time: '11 jam lalu',
      messageDetail: 'Pengajuan status overwrite pada [Transaction ID]/[User CIF] ditolak oleh supervisor',
    },
    {
      message: 'Pengajuan Status Overwrite Disetujui​',
      time: '1 jam lalu',
      messageDetail: 'Pengajuan status overwrite pada [Transaction ID]/[User CIF] disetujui oleh supervisor​',
    },
    {
      message: 'Pengajuan Status Overwrite​​',
      time: '1 menit lalu',
      messageDetail: 'Pengajuan status overwrite pada [Transaction ID]/[User CIF] sudah berhasil diajukan​',
    },
  ];

  displayNotification: boolean = false;

  displayNotificationButton() {
    this.displayNotification = true;
  }

  cutArrayNotification(data: any) {
    this.notificationArray.splice(data, 1);
  }

    items!: MenuItem[];
    
    profileItems: MenuItem[] = [
        {
            label: 'Profile',
            items: [
                {
                    label: 'Setting',
                    icon: 'pi pi-cog',
                    routerLink: '/profile-setting'
                },
                {
                    label: 'Log Out',
                    icon: 'pi pi-sign-out',
                    command: () => {
                        localStorage.removeItem('token');
                        this.router.navigateByUrl('/login');
                        setTimeout(() => {
                            location.reload()
                        }, 80);
                    }
                }
            ]
        }
    ];

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService, private readonly router:Router) { }
    
    onLogOut(){
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login');
        setTimeout(() => {
            location.reload()
        }, 80);
    }
}
