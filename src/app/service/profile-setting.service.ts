import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ProfileSettingService{

  constructor(private http: HttpClient) { }

  GetListUserById(body: any): Observable<any> {
    return this.http.post<any>(`user/view_by_id`, body);
  }

  GetUpdateUser(body: any): Observable<any> {
    return this.http.post<any>(`user/user_update`, body);
  }

  GetListGroup(body: any): Observable<any> {
    return this.http.post<any>(`group/view`, body);
  }

}