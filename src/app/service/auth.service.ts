import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService{
  constructor(private readonly http: HttpClient) { }

  public async signIn(user: any) {
    if (user.username_email == 'admin@mail.com' && user.password == 'admin123') {
      return {
        data:{
          username:  "Admin",
          token : "Bearer admin",
          session_token: 'tokenadmin'
        }
      };      
    } else {
      return {};
    }
  }

}
